<?php

// 1. Create code.php and index.php inside the activity folder.
// 2. In code.php, create a function named printDivisibleOfFive that will perform the following:
//     - Using loops, print all numbers that are divisible by 5 from 0 to 1000.
//     - Stop the loop when the loop reaches its 100th iteration.
// 3. Invoke the printDivisibleOfFive() function in the index.php
// 4. In code.php again, create an empty array named "students".
// 5. In index.php, perform the following using array functions:
//     - Accept a name of the student and add it to the "students" array.
//     - Print the names added so far in the "students" array.
//     - Count the number of names in the "students" array.
//     - Add another student then print the array and its new count.
//     - Finally, remove the firs student and print the array and its count.


// Function for Printing numbers divisible by 5 and stop at 100
function printDivisibleOfFive()
{
    for ($number = 0; $number <= 1000; $number++) {
        if ($number % 5 === 0) {
            echo $number . ' ';
        }
        if ($number >= 100) {
            break;
        }
    }
}

// Initial Array
$students = [];
