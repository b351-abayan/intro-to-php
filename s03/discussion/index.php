<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP Classes and Objects</title>
</head>

<body>
    <h1>Classes and Objects</h1>

    <h2>Object as Variables</h2>
    <pre><?php var_dump($buildingObj); ?></pre>

    <p>Building Name: <?= $buildingObj->name; ?></p>

    <h2>Objects from Classes</h2>

    <pre><?php var_dump($building); ?></pre>

    <!-- Accessing the printName() method of the instantiated object. -->
    <p>printName() Method: <?= $building->printName(); ?></p>

    <h1>Inheritance and Polymorphism</h1>

    <h2>Inheritance (Condominium Object)</h2>
    <pre><?php var_dump($condominium) ?></pre>

    <p>Condominium name: <?= $condominium->name; ?></p>
    <p>Condominium floors: <?= $condominium->floor; ?></p>

    <h2>Polymorphism (Overriding the behavior of the printName() method)</h2>
    <p>printName() Method fo Condominium: <?= $condominium->printName(); ?></p>
</body>

</html>