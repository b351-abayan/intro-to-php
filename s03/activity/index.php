<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity code s03</title>
</head>

<body>
    <h1>Person</h1>
    <!-- Invoking method for Person class -->
    <?= $person->printName(); ?>

    <!-- Invoking method for Developer class -->
    <h1>Developer</h1>
    <?= $developer->printName(); ?>

    <!-- Invoking method for Engineer class -->
    <h1>Engineer</h1>
    <?= $engineer->printName(); ?>
</body>

</html>