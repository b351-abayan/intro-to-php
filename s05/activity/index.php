<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity code s05</title>
</head>

<body>
    <!-- Session Initialization -->
    <?php session_start(); ?>

    <?php
    // Display the form if email is null
    if (!isset($_SESSION['email'])) :
    ?>
        <form action="./server.php" method="POST">
            <input type="hidden" name="action" value="login">
            <label for="">Email</label>
            <input type="email" name="username" required>
            <label for="">Password</label>
            <input type="password" name="password" required>
            <button type="submit">Login</button>
        </form>
    <?php
    else :
    ?>
        <!-- Display Greeting and logout if $_SESSION['email'] is not null -->
        Hello, <?= $_SESSION['email'] ?>
        <form action="./server.php" method="POST">
            <input type="hidden" name="action" value="logout">
            <button type="submit">Logout</button>
        </form>
    <?php
    endif; ?>

    <!-- Display error message if wrong credentials -->
    <?php if (isset($_SESSION['login_error_message'])) : ?>
        <p><?= $_SESSION['login_error_message'] ?></p>
    <?php endif; ?>

</body>

</html>