<?php

// Function for getting the Full Address
function getFullAddress($country, $city, $province, $specificAddress)
{
    return "$country, $province, $city, $specificAddress";
}

// Function for getting the Grade Letter
function getLetterGrade($grade)
{
    try {
        if (gettype($grade) === "integer") {
            if ($grade >= 89 && $grade === 100) {
                return "$grade is equivalent to A+";
            } else if ($grade >= 95 && $grade <= 97) {
                return "$grade is equivalent to A";
            } else if ($grade >= 92 && $grade <= 94) {
                return "$grade is equivalent to A-";
            } else if ($grade >= 89 && $grade <= 91) {
                return "$grade is equivalent to B+";
            } else if ($grade >= 86 && $grade <= 88) {
                return "$grade is equivalent to B";
            } else if ($grade >= 83 && $grade <= 85) {
                return "$grade is equivalent to B-";
            } else if ($grade >= 80 && $grade <= 82) {
                return "$grade is equivalent to C+";
            } else if ($grade >= 77 && $grade <= 79) {
                return "$grade is equivalent to C";
            } else if ($grade >= 75 && $grade <= 76) {
                return "$grade is equivalent to C-";
            } else {
                return "$grade is equivalent to F";
            }
        } else {
            throw new Exception('Value is a typeof ' . gettype($grade) . '. Input number from 1-100');
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
