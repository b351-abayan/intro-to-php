<?php
/*
		To start the PHP Development Server in the terminal:

		php -S localhost:8000
	*/

// [SECTION] Comments
// Comments are meant to describe the written code.

/*
			There are two types of comments:
			- Single line comment denoted by two forward slashes.
			- Multi-line comment denoted by a forward slash and asterisk 

		*/

// [SECTION] Variables
// Variables are defined using the dollar($) sign notation before the variable name.

$name = 'John Smith';
$email = "johnsmith@gmail";

// [SECTION] Constants
// Constants are defined using the define() function.
// Naming convetions for "constant" should be in ALL CAPS.
// It doesn't use the "$" notation before the variable name.

define("PI", 3.1416);

// [SECTION] Data Types

// Strings
$state = "New York";
$country = 'United States of America';
$address = $state . ', ' . $country; //concatetnation via . notation
// $address = "$state, $country"; concatention via double quote.

// Integers
$age = 31;
$headcount = 26;

// Floats
$grade = 98.2;
$distanceInKilometers = 1342.12;

// Boolean
$hasTravelledAbroad = false;
$haveSymptoms = true;

// Arrays
// array() is used to create an array in PHP.
// '[]' will also work in creating array.
$grades = array(98.7, 92.1, 90.2, 94.6);

// Null
$spouse = null;
$middleName = null;

// Objects
// Objects in PHP are used to model more complex data structures and encapsulate data and behavior.
// fat arrow (=>) is used to assign key value pairs.

$gradesObj = (object)[
    'firstGrading' => 98.7,
    'secondGrading' => 92.1,
    'thirdGrading' => 90.2,
    'fourthGrading' => 94.6
];

$personObj = (object)[
    'fullName' => 'John Smith',
    'isMarried' => false,
    'age' => 35,
    'address' => (object)[
        'state' => 'New York',
        'country' => 'United States of America'
    ]
];
