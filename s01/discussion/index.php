<!-- This is used to includes another php file. -->
<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP Basics and Selection Control Structure</title>
</head>

<body>
    <!-- <h1>Hello World!</h1> -->

    <h1>Echoing Values</h1>

    <!-- It can easily read variables -->
    <p><?php echo "Good day $name! Your given email is $email."; ?></p>

    <p><?php echo 'Good day $name! Your given email is $email.'; ?></p>

    <!-- Single quote can be used but concatenation is needed -->
    <!-- dot(.) is used for concatenation. -->
    <p><?php echo 'Good day' . $name . '!' . 'Your given email is' . $email . '.'; ?></p>

    <!-- Constant variable uses the dot(.) to display its value. -->
    <p><?php echo "The value of PI is " . PI; ?></p>

    <h1>Data Types</h1>

    <!-- Boolean and Null variable will not be visible using echo -->
    <p>hasTravelledAbroad: <?php echo "$hasTravelledAbroad"; ?></p>
    <p>spouse: <?php echo "$spouse"; ?></p>

    <!-- To see their types instead, we can use gettype() or vardump() function. -->

    <!-- gettype() function -->
    <p>hasTravelledAbroad: <?php echo gettype($hasTravelledAbroad); ?></p>
    <p>spouse: <?php echo gettype($spouse); ?></p>

    <!-- var_dump() function -->
    <p>hasTravelledAbroad: <?php var_dump($hasTravelledAbroad); ?></p>
    <p>spouse: <?php var_dump($spouse); ?></p>

    <!-- To output the value of an object property, the arrow notation can be used. -->
    <p><?php echo $gradesObj->firstGrading; ?></p>
    <p><?php echo $personObj->address->state; ?></p>

    <!-- To output an array element, we can use the usual square brackets. -->
    <p><?php echo $grades[3]; ?></p>
    <p><?php echo $grades[2]; ?></p>

    <!-- If we want to display the whole array and object we can use var_dump() -->
    <p><?php var_dump($gradesObj); ?></p>
    <p><?php var_dump($grades); ?></p>

    <!-- print_r() function can also be used to print the whole array and object -->
    <p><?php print_r($personObj); ?></p>
    <p><?php print_r($grades); ?></p>
</body>

</html>